#!/usr/bin/env php
<?php
/**
 * @file
 * Several logic pieces for analyzer project.
 */

require_once 'vendor/autoload.php';

use DrupalContributionAnalyzer\Console\Command\MakefileGeneratorCommand;
use DrupalContributionAnalyzer\Console\Command\CodeswarmConfigGeneratorCommand;
use DrupalContributionAnalyzer\Console\Command\StatsHtmlGeneratorCommand;
use DrupalContributionAnalyzer\Console\Command\DbGeneratorCommand;
use DrupalContributionAnalyzer\Console\Command\CodeswarmXmlGeneratorCommand;
use DrupalContributionAnalyzer\Console\Command\StatsYmlGeneratorCommand;
use DrupalContributionAnalyzer\Console\Command\GraphsHtmlGeneratorCommand;
use DrupalContributionAnalyzer\Console\Command\InterDbGraphsHtmlGeneratorCommand;
use Symfony\Component\Console\Application;

$console = new Application();
$console->add(new MakefileGeneratorCommand);
$console->add(new CodeswarmConfigGeneratorCommand);
$console->add(new StatsHtmlGeneratorCommand);
$console->add(new DbGeneratorCommand);
$console->add(new CodeswarmXmlGeneratorCommand);
$console->add(new StatsYmlGeneratorCommand);
$console->add(new GraphsHtmlGeneratorCommand);
$console->add(new InterDbGraphsHtmlGeneratorCommand);
$console->run();
