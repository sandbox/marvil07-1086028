<?php

namespace DrupalContributionAnalyzer\Console\Command;

use DrupalContributionAnalyzer\DbAnalyzer;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Produces some graphs based on the provided database.
 */
class GraphsHtmlGeneratorCommand extends GraphsHtmlGeneratorCommandBase
{

    protected function configure()
    {
        parent::configure();
        $this
            ->setName('gengraphshtml')
            ->setDescription('Generate some grapsh in HTML.')
            ->addArgument(
                'sqlite-database-file',
                InputArgument::REQUIRED,
                'The database file keyed by name.
                e.g. "7.x:file1.sqlite3"'
            )
            ->twigConfigure();
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        parent::execute($input, $output);
        $this->twigValidate($input);
        $db_file_value = $input->getArgument('sqlite-database-file');
        $info = $this->getDatabaseInformation(array($db_file_value));
        foreach ($info as $id => $db_file) {
            $this->dbAnalyzers[$id] = new DbAnalyzer($db_file);
        }
        $this->generateCommitsPerMonth();
        $this->generateContributorsPerMonth();
        $this->generateCommitsPerContributorHistogram();
        $this->generateFileChangesPerContributorHistogram();
        $this->generateContributorsPerCommitHistogram();
        $this->generateChangedFilesPerCommitHistogram();
        $variables = array();
        if (!$scenario = $input->getOption('identifier')) {
            $scenario = array_pop(array_keys($info));
        }
        $variables['title'] = "Graphs for $scenario";
        $variables['graphs'] = $this->graphs;
        $this->twigRender($output, 'graphs.html', $variables);
    }

}
