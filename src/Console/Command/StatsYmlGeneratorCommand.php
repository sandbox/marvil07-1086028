<?php

namespace DrupalContributionAnalyzer\Console\Command;

use DrupalContributionAnalyzer\DbAnalyzer;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Produces a stats YML version based on an indicator.
 *
 * Note: Not using symfony Yaml class dumper to avoid quotes on yml strings,
 * which then appear on tagclouds.
 */
class StatsYmlGeneratorCommand extends Command
{

    /**
     * Object to interect with related databse.
     *
     * @var DbAnalyzer
     */
    protected $dbAnalyzer;

    protected function configure()
    {
        $this
            ->setName('genstatsyml')
            ->setDescription('Generates a stats YML based on an indicator.')
            ->addArgument(
                'sqlite-database-file',
                InputArgument::REQUIRED,
                'The database file.'
            )
            ->addArgument(
                'stat-name',
                InputArgument::REQUIRED,
                'The desired stat: file-changes or commits.'
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $db_file = $input->getArgument('sqlite-database-file');
        $this->dbAnalyzer = new DbAnalyzer($db_file);
        switch ($stat_name = $input->getArgument('stat-name')) {
            case 'commits':
                $stats = $this->generateCommitsStat();
                break;
            case 'file-changes':
                $stats = $this->generateFileChangesStat();
                break;
            default:
                throw new \InvalidArgumentException(sprintf('Unsupported stat-name (%s)', $stat_name));
        }
        $output->write($stats);
    }

    /**
     * Participation by commits.
     *
     * @return string
     *   YAML string to output.
     */
    protected function generateCommitsStat()
    {
        return $this
            ->dbAnalyzer
            ->getDb()
            ->query(DbAnalyzer::QUERY_COMMITS_BY_CONTRIBUTOR)
            ->fetchAll(\PDO::FETCH_FUNC, array($this, 'formatStats'));
    }

    /**
     * Participation by file changes.
     *
     * @return string
     *   YAML string to output.
     */
    protected function generateFileChangesStat()
    {
        return $this
            ->dbAnalyzer
            ->getDb()
            ->query(DbAnalyzer::QUERY_FILECHANGES_BY_CONTRIBUTOR)
            ->fetchAll(\PDO::FETCH_FUNC, array($this, 'formatStats'));
    }

    /**
     * Callback for fetchAll.
     */
    public function formatStats($contributor, $value)
    {
        return "{$contributor}: {$value}" . PHP_EOL;
    }

}
