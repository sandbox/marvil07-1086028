<?php

namespace DrupalContributionAnalyzer\Console\Command;

use DrupalContributionAnalyzer\TwigAwareCommandTrait;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Yaml\Yaml;

/**
 * Produces an HTML version of the base stats file.
 */
class StatsHtmlGeneratorCommand extends Command
{

    use TwigAwareCommandTrait;

    protected function configure()
    {
        $this
            ->setName('genstatshtml')
            ->setDescription('Generate an HTML version of a stats file.')
            ->addArgument(
                'stats-file',
                InputArgument::REQUIRED,
                'The stats file.'
            )
            ->twigConfigure();
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->twigValidate($input);
        if ($stats_file = $input->getArgument('stats-file')) {
            if (!file_exists($stats_file)) {
                throw new \InvalidArgumentException(sprintf('stats-file (%s) does not exists', $stats_file));
            }
        }
        $variables = array();
        $raw_stats = Yaml::parse($stats_file);
        // Sort by count desc.
        arsort($raw_stats);
        $variables['stats'] = array();
        $variables['title'] = $stats_file;
        foreach ($raw_stats as $name => $count) {
            $variables['stats'][] = array(
                'name' => $name,
                'count' => $count,
            );
        }
        $this->twigRender($output, 'stats.html', $variables);
    }

}
