<?php

namespace DrupalContributionAnalyzer\Console\Command;

use DrupalContributionAnalyzer\DbAnalyzer;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Produces some graphs based on the provided database.
 */
class InterDbGraphsHtmlGeneratorCommand extends GraphsHtmlGeneratorCommandBase
{

    protected function configure()
    {
        parent::configure();
        $this
            ->setName('geninterdbgraphshtml')
            ->setDescription('Generate some inter-databases graphs in HTML.')
            ->addArgument(
                'sqlite-database-files',
                InputArgument::IS_ARRAY | InputArgument::REQUIRED,
                'A list of database files to use for the comparison keyed by branch name.
                e.g. "7.x:file1.sqlite3 8.x:file2.sqlite3"',
                array()
            )
            ->twigConfigure();
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        parent::execute($input, $output);
        $db_files = $input->getArgument('sqlite-database-files');
        if (count($db_files) < 2) {
            throw new \InvalidArgumentException('sqlite-database-files: At least two files are required to run this command.');
        }
        foreach ($this->getDatabaseInformation($db_files) as $branch_id => $db_file) {
            $this->dbAnalyzers[$branch_id] = new DbAnalyzer($db_file);
        }
        $this->generateCommitsPerMonth();
        $this->generateContributorsPerMonth();
        $this->generateCommitsPerContributorHistogram();
        $this->generateFileChangesPerContributorHistogram();
        $this->generateContributorsPerCommitHistogram();
        $this->generateChangedFilesPerCommitHistogram();
        $variables = array();
        if (!$identifier = $input->getOption('identifier')) {
            $identifier = 'Inter-databases graphs';
        }
        $variables['title'] = $identifier;
        $variables['graphs'] = $this->graphs;
        $this->twigRender($output, 'graphs.html', $variables);
    }

}
