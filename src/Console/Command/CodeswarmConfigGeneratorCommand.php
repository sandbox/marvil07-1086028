<?php

namespace DrupalContributionAnalyzer\Console\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Yaml\Yaml;

/**
 * Produces a codeswarm config file based on general configuration.
 */
class CodeswarmConfigGeneratorCommand extends Command
{

    /**
     * YAML loaded data from config-file argument.
     *
     * @var array
     */
    protected $configuration;

    /**
     * Internal makefile representation.
     *
     * @var array
     */
    protected $makefile = array(
        'variables' => array(),
        'targets' => array(),
    );

    protected function configure()
    {
        $this
            ->setName('gencodeswarmconfig')
            ->setDescription('Generate a codeswarm configuration file.')
            ->addArgument(
                'config-file',
                InputArgument::REQUIRED,
                'The YAML configuration file passed to genmakefile.'
            )
            ->addArgument(
                'id',
                InputArgument::REQUIRED,
                'Identifier of this configuration.'
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if ($configuration_file = $input->getArgument('config-file')) {
            if (!file_exists($configuration_file)) {
                throw new \InvalidArgumentException(sprintf('config-file (%s) does not exists', $configuration_file));
            }
            $this->configuration = Yaml::parse($configuration_file);
        }
        $this->createCodeswarmConfigFile($input->getArgument('id'), $output);
    }

    /**
     * Adds codeswarm related targets to makefile.
     */
    protected function createCodeswarmConfigFile($id, OutputInterface $output)
    {
        $codeswarm_config = $this->createCodeswarmConfig($id);
        foreach ($codeswarm_config as $name => $value) {
            $output->writeln(sprintf('%s=%s', $name, $value));
        }
    }

    /**
     * Build an internal representation of the codeswarm configuration file.
     *
     * @todo Support per branch overrides.
     */
    protected function createCodeswarmConfig($id)
    {
        $codeswarm_config = array();
        $simple_variable_defaults = array(
            'width' => array('variable_name' => 'Width', 'value' => 800),
            'height' => array('variable_name' => 'Height', 'value' => 600),
            'font' => array('variable_name' => 'Font', 'value' => 'SansSerif'),
            'font_size' => array('variable_name' => 'FontSize', 'value' => 12),
            'bold_font_size' => array('variable_name' => 'BoldFontSize', 'value' => 18),
            'miliseconds_per_frame' => array('variable_name' => 'MillisecondsPerFrame', 'value' => '21600000'),
            'max_threads' => array('variable_name' => 'MaxThreads', 'value' => 1),
        );
        foreach ($simple_variable_defaults as $yaml_key => $default) {
            if (!empty($this->configuration['codeswarm'][$yaml_key])) {
                $codeswarm_config[$default['variable_name']] = $this->configuration['codeswarm'][$yaml_key];
            }
            else {
                $codeswarm_config[$default['variable_name']] =  $default['value'];
            }
        }

        // Colors.
        if (!empty($this->configuration['codeswarm']['colors'])) {
          $counter = 1;
          foreach ($this->configuration['codeswarm']['colors'] as $color_data) {
            $codeswarm_config['ColorAssign' . $counter] = sprintf('"%s", "%s", %s, %s',
              $color_data['label'],
              $color_data['regex'],
              $color_data['color'],
              $color_data['color']
            );
            ++$counter;
          }
        }

        // Non-overridable configuration.
        $codeswarm_config += array(
            'InputFile' => sprintf('%s.xml', $id),
            'ParticleSpriteFile' => 'src/particle.png',
            'Background' => '0,0,0',
            'TakeSnapshots' => 'true',
            'SnapshotLocation' => sprintf('frames/%s/frame-######.png', $id),
            'DrawNamesSharp' => 'true',
            'DrawNamesHalos' => 'false',
            'DrawFilesSharp' => 'true',
            'DrawFilesFuzzy' => 'false',
            'DrawFilesJelly' => 'false',
            'ShowLegend' => 'true',
            'ShowHistory' => 'true',
            'ShowDate' => 'true',
            'ShowEdges' => 'false',
            'ShowDebug' => 'false',
            'EdgeLength' => '45',
            'EdgeDecrement' => '-8',
            'FileDecrement' => '-8',
            'PersonDecrement' => '-12',
            'FileSpeed' => '7.0',
            'PersonSpeed' => '0.7',
            'FileMass' => '1.0',
            'PersonMass' => '10.0',
            'EdgeLife' => '250',
            'FileLife' => '200',
            'PersonLife' => '255',
            'HighlightPct' => '10',
            'PhysicsEngineConfigDir' => 'physics_engine',
            'PhysicsEngineSelection' => 'PhysicsEngineLegacy',
            'UseOpenGL' => 'false',
        );
        return $codeswarm_config;
    }

}
