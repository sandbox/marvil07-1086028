<?php

namespace DrupalContributionAnalyzer\Console\Command;

use DrupalContributionAnalyzer\Config;
use DrupalContributionAnalyzer\DbAnalyzer;
use DrupalContributionAnalyzer\TwigAwareCommandTrait;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Base class for html pages containing graphs.
 */
abstract class GraphsHtmlGeneratorCommandBase extends Command
{

    use TwigAwareCommandTrait;

    /**
     * List of objects to interect with related databses.
     *
     * @var array(DbAnalyzer)
     */
    protected $dbAnalyzers;

    /**
     * Graph arrays.
     *
     * @var array
     */
    protected $graphs = array();

    /**
     * Flag to to exclude maintainers from stats.
     *
     * @var boolean
     */
    protected $excludeMaintainers = FALSE;

    /**
     * Configuration object.
     *
     * @var \DrupalContributionAnalyzer\Config
     */
    protected $configuration;

    protected function configure()
    {
        $this
            ->addOption(
                'exclude-maintainers',
                'e',
                InputOption::VALUE_NONE,
                'If provided, related maintainers will be excluded from retrieved data. This can help visualize contributions when maintainers has a lot of contributions compared with non-maintainers, e.g. core.'
            )
            ->addOption(
                'identifier',
                'i',
                InputOption::VALUE_OPTIONAL,
                'A name to identify the graphs html page.',
                null
            )
            ->addOption(
                'config-file',
                'c',
                InputOption::VALUE_OPTIONAL,
                'Path to main configuration file.',
                null
            )
            ->twigConfigure();
    }

    /**
     * Parses name:filename.sqlite on a list of strings.
     *
     * @param array $db_files
     *   List of arguments in branch_id:filename.ext format.
     *
     * @return array
     *   Keys are branch ids and values are database filenames.
     */
    protected function getDatabaseInformation($db_files) {
        $info = array();
        foreach ($db_files as $value) {
            if (strpos($value, ':') === FALSE) {
                throw new \RuntimeException('database files should be in the format "branch_id:filename"');
            }
            else {
                list($branch_id, $db_file) = explode(':', $value);
            }
            $info[$branch_id] = $db_file;
        }
        return $info;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->twigValidate($input);
        if ($input->getOption('exclude-maintainers')) {
            $this->excludeMaintainers = TRUE;
            $this->configuration = new Config($input->getOption('config-file'));
        }
    }

    /**
     * Helper to get the right query.
     *
     * @param \DrupalContributionAnalyzer\DbAnalyzer $db_analyzer
     *   Related object used to generte the query.
     * @param string $query_type
     *   One of the \DrupalContributionAnalyzer\DbAnalyzer::QUERY_* constants.
     * @param string $branch_id
     *   Branch id.
     *
     * @return \PDOStatement
     *   Statement with the asked query.
     */
    protected function getQuery($db_analyzer, $query_type, $branch_id) {
        if ($this->excludeMaintainers) {
            $maintainers = $this->configuration->getBranchMaintainers($branch_id);
            $query = $db_analyzer->getNoMaintainersQuery($query_type, $maintainers);
        }
        else {
            $query = $db_analyzer
                ->getDb()
                ->query($query_type);
        }
        return $query;
    }

    protected function generateCommitsPerContributorHistogram() {
        $graph = array(
            'id' => 'commits_per_contributor_frequency',
            'title' => 'Commits per contributor histogram',
            'xlabel' => 'Commits per contributor',
            'ylabel' => 'Frequency',
            'datasets' => array(),
        );
        foreach ($this->dbAnalyzers as $branch_id => $db_analyzer) {
            $query = $this->getQuery($db_analyzer, DbAnalyzer::QUERY_COMMITS_BY_CONTRIBUTOR, $branch_id);
            $dataset = array(
                'type' => 'bars',
                'label' => $branch_id,
                'points' => $this->generateHistogramData($query),
            );
            $graph['datasets'][] = $dataset;
        }
        $this->graphs[] = $graph;
    }

    protected function generateFileChangesPerContributorHistogram() {
        $graph = array(
            'id' => 'file_changes_per_contributor_frequency',
            'title' => 'File changes per contributor histogram',
            'xlabel' => 'File changes per contributor',
            'ylabel' => 'Frequency',
            'datasets' => array(),
        );
        foreach ($this->dbAnalyzers as $branch_id => $db_analyzer) {
            $query = $this->getQuery($db_analyzer, DbAnalyzer::QUERY_FILECHANGES_BY_CONTRIBUTOR, $branch_id);
            $dataset = array(
                'type' => 'bars',
                'label' => $branch_id,
                'points' => $this->generateHistogramData($query),
            );
            $graph['datasets'][] = $dataset;
        }
        $this->graphs[] = $graph;
    }

    protected function generateContributorsPerCommitHistogram() {
        $graph = array(
            'id' => 'contributors_per_commit_frequency',
            'title' => 'Contributors per commit histogram',
            'xlabel' => 'Contributors per commit',
            'ylabel' => 'Frequency',
            'datasets' => array(),
        );
        foreach ($this->dbAnalyzers as $branch_id => $db_analyzer) {
            $query = $this->getQuery($db_analyzer, DbAnalyzer::QUERY_CONTRIBUTORS_BY_COMMIT, $branch_id);
            $dataset = array(
                'type' => 'bars',
                'label' => $branch_id,
                'points' => $this->generateHistogramData($query),
            );
            $graph['datasets'][] = $dataset;
        }
        $this->graphs[] = $graph;
    }

    protected function generateChangedFilesPerCommitHistogram() {
        $graph = array(
            'id' => 'changed_files_per_commit_frequency',
            'title' => 'File changes per commit histogram',
            'xlabel' => 'File changes per commit',
            'ylabel' => 'Frequency',
            'datasets' => array(),
        );
        foreach ($this->dbAnalyzers as $branch_id => $db_analyzer) {
            $query = $this->getQuery($db_analyzer, DbAnalyzer::QUERY_FILECHANGES_BY_COMMIT, $branch_id);
            $dataset = array(
                'type' => 'bars',
                'label' => $branch_id,
                'points' => $this->generateHistogramData($query),
            );
            $graph['datasets'][] = $dataset;
        }
        $this->graphs[] = $graph;
    }

    protected function generateCommitsPerMonth() {
        $graph = array(
            'id' => 'commits_per_month',
            'title' => 'Commits per month',
            'xlabel' => 'Time',
            'ylabel' => 'Commits',
            'timed' => TRUE,
            'datasets' => array(),
        );
        foreach ($this->dbAnalyzers as $branch_id => $db_analyzer) {
            $query = $this->getQuery($db_analyzer, DbAnalyzer::QUERY_COMMITS_PER_MONTH, $branch_id);
            $dataset = array(
                'type' => 'lines',
                'label' => $branch_id,
                'points' => $this->generateTimeData($query),
            );
            $graph['datasets'][] = $dataset;
        }
        $this->graphs[] = $graph;
    }

    protected function generateContributorsPerMonth() {
        $graph = array(
            'id' => 'contributors_per_month',
            'title' => 'Contributors per month',
            'xlabel' => 'Time',
            'ylabel' => 'Contributors',
            'timed' => TRUE,
            'datasets' => array(),
        );
        foreach ($this->dbAnalyzers as $branch_id => $db_analyzer) {
            $query = $this->getQuery($db_analyzer, DbAnalyzer::QUERY_CONTRIBUTORS_PER_MONTH, $branch_id);
            $dataset = array(
                'type' => 'lines',
                'label' => $branch_id,
                'points' => $this->generateTimeData($query),
            );
            $graph['datasets'][] = $dataset;
        }
        $this->graphs[] = $graph;
    }

    /**
     * Generates histogram data.
     *
     * @param \PDOStatement $query
     *   Related query statement object.
     *
     * @return array
     *   Keys are possible values, values are frecuency.
     */
    protected function generateHistogramData($query) {
        $query->execute();
        $results = $query->fetchAll();
        $frequency = array();
        foreach ($results as $result) {
            if (!isset($frequency[$result['value']])) {
                $frequency[$result['value']] = 0;
            }
            ++$frequency[$result['value']];
        }
        return $frequency;
    }

    /**
     * Generates time data as expected by flot.
     *
     * @todo Add start/end, so 0 can be shown in lines graphs.
     *
     * @param \PDOStatement $query
     *   An sql select query containing fields:
     *   - value: Value of a given point.
     *   - date_point: timestamp to aggregate.
     *
     * @return array
     *   Keys are js microtimestamps, values are data values.
     */
    protected function generateTimeData($query) {
        $query->execute();
        $results = $query->fetchAll();
        $data = array();
        foreach ($results as $result) {
            $data[$result['date_point'] * 1000] = $result['value'];
        }
        return $data;
    }

}
