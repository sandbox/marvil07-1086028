<?php

namespace DrupalContributionAnalyzer\Console\Command;

use DrupalContributionAnalyzer\DbAnalyzer;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Yaml\Yaml;

/**
 * Produces an sqlite database file of contributions.
 *
 * Database contains one table named contributions. Its columns are:
 * commit_hash, author_timestamp, filename, contributor.
 */
class DbGeneratorCommand extends Command
{

    /**
     * libgit2 resource for related git repository.
     *
     * @var resource(git2).
     */
    protected $repository;

    /**
     * See drupal-git-path argument documentation.
     *
     * @var string
     */
    protected $gitPath;

    /**
     * Object to interect with related databse.
     *
     * @var DbAnalyzer
     */
    protected $dbAnalyzer;

    /**
     * Map of commit message overrides.
     *
     * Keys are commit hashes and values are commit messages.
     *
     * @var array
     */
    protected $commitOverrideMap = array();

    /**
     * Maps mails to contirbutors.
     *
     * Keys are mails and values are contributors.
     *
     * @var array
     */
    protected $mailMap = array();

    /**
     * See ref-interval option documentation.
     *
     * @var string
     */
    protected $refInterval;

    protected function configure()
    {
        $this
            ->setName('gendb')
            ->setDescription('Generate a sqlite database with contributions')
            ->addArgument(
                'drupal-git-path',
                InputArgument::REQUIRED,
                'Local bare clone of the drupal git repository'
            )
            ->addArgument(
                'ref-interval',
                InputArgument::REQUIRED,
                'A git ref interval, i.e. <since>..<until>; or the string "all" for full history.'
            )
            ->addArgument(
                'output-sqlite-db-file',
                InputArgument::REQUIRED,
                'Where to write the sqlite database file.'
            )
            ->addOption(
                'commit-override-file',
                'c',
                InputOption::VALUE_OPTIONAL,
                'YAML file overriding some commit messages by hash, format is:
                hash: |
                  Modified message here.',
                null
            )
            ->addOption(
                'mail-to-contributor-file',
                'm',
                InputOption::VALUE_OPTIONAL,
                'YAML file mapping mails with contributor names, format is:
                mail: name',
                null
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $db_file = $input->getArgument('output-sqlite-db-file');
        $this->gitPath = $input->getArgument('drupal-git-path');
        if (!$this->repository = git_repository_open_bare($this->gitPath)) {
          throw new \InvalidArgumentException(sprintf('drupal-git-path argument (%s) is not a git bare repository', $this->gitPath));
        }
        if ($commit_override_file = $input->getOption('commit-override-file')) {
            if (!file_exists($commit_override_file)) {
                throw new \InvalidArgumentException(sprintf('commit-override-file (%s) does not exists', $commit_override_file));
            }
            $this->commitOverrideMap = Yaml::parse($commit_override_file);
        }
        if ($mail_to_contributor_file = $input->getOption('mail-to-contributor-file')) {
            if (!file_exists($mail_to_contributor_file)) {
                throw new \InvalidArgumentException(sprintf('mail-to-contributor-file (%s) does not exists', $mail_to_contributor_file));
            }
            $this->mailMap = Yaml::parse($mail_to_contributor_file);
        }
        $this->dbAnalyzer = DbAnalyzer::create($db_file);
        $this->refInterval= $input->getArgument('ref-interval');
        $this->revWalk($output);
    }

    /**
     * Iterates over ref interval and print relevant data.
     */
    protected function revWalk(OutputInterface $output)
    {
      // See libgit2 GIT_SORT_XXX flags.
      $git_sort_topological = 1 << 0;
      $git_sort_reverse = 1 << 2;

      $walker = git_revwalk_new($this->repository);
      git_revwalk_sorting($walker, $git_sort_reverse | $git_sort_topological);
      if ($this->refInterval == 'all') {
          git_revwalk_push_glob($walker, '*');
      }
      elseif (git_revwalk_push_range($walker, $this->refInterval) != 0) {
          throw new \InvalidArgumentException(sprintf('ref-interval argument (%s) is not a valid ref interval', $this->refInterval));
      }
      $statement = $this
          ->dbAnalyzer
          ->getDb()
          ->prepare('INSERT INTO contributions (commit_hash, author_timestamp, filename, contributor) VALUES (:commit_hash, :author_timestamp, :filename, :contributor)');
      $this->dbAnalyzer
          ->getDb()
          ->beginTransaction();
      while ($commit_hash = git_revwalk_next($walker)) {
          $commit_contributors = array();
          $commit = git_commit_lookup($this->repository, $commit_hash);
          $author = git_commit_author($commit);
          $author_timestamp = $author['time']->getTimestamp();
          $message = $this->getCommitMessage($commit_hash, $commit);
          // Use mail to identify committer/author contributors
          $author_contributor = $this->getContributorFromMail($author['email']);
          $commit_contributors[] = $author_contributor;
          $commiter = git_commit_committer($commit);
          $commiter_contributor = $this->getContributorFromMail($commiter['email']);
          if ($author_contributor != $commiter_contributor) {
              // Commiter and author are different.
              $commit_contributors[] = $commiter_contributor;
          }
          foreach ($this->getContributorsFromMessage($message) as $in_message_contributor) {
              if (in_array($in_message_contributor, $commit_contributors)) {
                  // Already added.
                  continue;
              }
              $commit_contributors[] = $in_message_contributor;
          }
          foreach ($this->getChangedFilenames($commit, $commit_hash) as $filename) {
              foreach ($commit_contributors as $contributor) {
                  $statement->execute(array(
                      ':commit_hash' => $commit_hash,
                      ':author_timestamp' => $author_timestamp,
                      ':filename' => $filename,
                      ':contributor' => $contributor,
                  ));
              }
          }
      }
      $this->dbAnalyzer
          ->getDb()
          ->commit();
    }

    /**
     * Retrieves one commit changed filenames.
     *
     * @todo Reimplement when 0.21 is supported by php binding, so we can use
     * git_diff_stats_files_changed().
     */
    protected function getChangedFilenames($commit, $commit_hash)
    {
        $filenames = array();
        $command = 'git --git-dir=' . escapeshellarg($this->gitPath) . ' whatchanged -1 --format="%an" ' . escapeshellarg($commit_hash);
        $output = '';
        exec($command, $output, $return_value);
        if ($return_value != 0) {
            return $filenames;
        }
        foreach ($output as $change) {
            if (strlen($change) < 1 || $change[0] != ':') {
                continue;
            }
            list(, $filename) = explode("\t", $change);
            $filenames[] = $filename;
        }
        return $filenames;
    }

    /**
     * Let override commit messages.
     */
    protected function getCommitMessage($commit_hash, $commit)
    {
        if (!empty($this->commitOverrideMap[$commit_hash])) {
            return $this->commitOverrideMap[$commit_hash];
        }
        return git_commit_message($commit);
    }

    /**
     * Maps mails to contributor names.
     */
    protected function getContributorFromMail($mail)
    {
        if (empty($mail)) {
            // Probably a commit to fix manually.
            return 'FIXME_nomail';
        }
        if (!empty($this->mailMap[$mail])) {
            return $this->mailMap[$mail];
        }
        return $mail;
    }

    /**
     * Extracts contributor names from commit message.
     */
    protected function getContributorsFromMessage($message)
    {
        $contributors = array();
        // Analyze 1st line for in-message attribution.
        $first_line = strtok($message, "\n");
        if (!$first_line) {
            // Nothing else to do.
            return $contributors;
        }
        // usernames are '/[^\x{80}-\x{F7} a-z0-9@_.\'-]/i'
        if (!preg_match('/\bby ([\x{80}-\x{F7} a-z0-9@_.\'-, ]*):/i', $first_line, $matches)) {
            // No extra contributors in message.
            return $contributors;
        }
        // @fixme Support users ending in period.
        $split_regex = '/[\s]*,[\s]*|[, ]+(and[\s]+|et al[l]?)/';
        foreach (preg_split($split_regex, $matches[1], -1,  PREG_SPLIT_NO_EMPTY) as $contributor_string) {
            $contributor_string = trim($contributor_string);
            $contributor_string = preg_replace('/^and[\s]+/', '', $contributor_string);
            $invalid_names = array('me', 'myself', 'friends', 'et al', 'et all');
            if (empty($contributor_string) || in_array($contributor_string, $invalid_names)) {
                // Skip empty and invalid.
                continue;
            }
            $contributors[] = $contributor_string;
        }
        return $contributors;
    }
}
