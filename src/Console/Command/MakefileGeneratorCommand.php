<?php

namespace DrupalContributionAnalyzer\Console\Command;

use DrupalContributionAnalyzer\Config;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Produces a Makefile based on configuration.
 *
 * @todo project to html titles.
 */
class MakefileGeneratorCommand extends Command
{

    const FEATURE_DB = 'db';
    const FEATURE_STATS = 'stats';
    const FEATURE_GRAPH_STATS = 'graphstats';
    const FEATURE_TAGCLOUDS = 'tagclouds';
    const FEATURE_CODESWARM = 'codeswarm';

    /**
     * Configuration object.
     *
     * @var \DrupalContributionAnalyzer\Config
     */
    protected $configuration;

    /**
     * Internal makefile representation.
     *
     * @var array
     */
    protected $makefile = array(
        'variables' => array(),
        'hard_variables' => array(),
        'targets' => array(),
    );

    protected function configure()
    {
        $this
            ->setName('genmakefile')
            ->setDescription('Generate a Makefile based on configuration file.')
            ->addArgument(
                'config-file',
                InputArgument::REQUIRED,
                'A YAML configuration file describing what to do. See projects/template_project/config/analyzer-documented.yml'
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->configuration = new Config($input->getArgument('config-file'));
        $this->buildMakefile();
        $this->writeMakefile($output);
    }

    /**
     * Determines if branch should have related feature.
     *
     * @param string $branch_id
     *   Either the git branch name or 'full' referring to full history
     *   scenario.
     * @param string $feature
     *   One of the self::FEATURE_* constants.
     *
     * @return boolean
     *   Feature should be enabled or not.
     */
    protected function hasFeature($branch_id, $feature)
    {
        return in_array($feature, $this->getFeatures($branch_id));
    }

    /**
     * Determines features to use for a branch.
     *
     * Adds dependencies if needed.
     *
     * @param string $branch_id
     *   Either the git branch name or 'full' referring to full history
     *   scenario.
     *
     * @return boolean
     *   Feature should be enabled or not.
     */
    protected function getFeatures($branch_id)
    {
        static $computed_features;
        if (isset($computed_features[$branch_id])) {
            return $computed_features[$branch_id];
        }
        $features_status = array(
            self::FEATURE_DB => 0,
            self::FEATURE_STATS => 0,
            self::FEATURE_GRAPH_STATS => 0,
            self::FEATURE_TAGCLOUDS => 0,
            self::FEATURE_CODESWARM => 0,
        );
        // By default all features are enabled.
        $default_features = array(
            self::FEATURE_DB,
            self::FEATURE_STATS,
            self::FEATURE_GRAPH_STATS,
            self::FEATURE_TAGCLOUDS,
            self::FEATURE_CODESWARM,
        );
        $full_dependencies_by_feature = array(
            self::FEATURE_DB => array(),
            self::FEATURE_STATS => array(self::FEATURE_DB),
            self::FEATURE_GRAPH_STATS => array(self::FEATURE_DB),
            self::FEATURE_TAGCLOUDS => array(self::FEATURE_DB, self::FEATURE_STATS),
            self::FEATURE_CODESWARM => array(self::FEATURE_DB),
        );
        $declared_features = $this->configuration->getBranchOption($branch_id, 'features', $default_features);
        $features = array();
        foreach ($declared_features as $feature) {
            if (!in_array($feature, array_keys($features_status))) {
                throw new \InvalidArgumentException(sprintf('config-file contains an invalid feature (%s)', $feature));
            }
            $features[$feature] = 1;
            foreach ($full_dependencies_by_feature[$feature] as $dependency) {
                $features[$dependency] = 1;
            }
        }
        $computed_features[$branch_id] = array_keys(array_filter($features));
        return $computed_features[$branch_id];
    }

    /**
     * Builds the internal representation of the makefile.
     */
    protected function buildMakefile()
    {
        // Fill default values.
        $this->buildBaseMakefile();
        // Full history.
        $full_history = TRUE;
        if ($this->configuration->getOption('skip_full_history', FALSE)) {
          $full_history = FALSE;
        }
        if ($full_history) {
          $this->newBranchTarget('full');
        }
        // Branches.
        if (!$branches = $this->configuration->getOption('branches', FALSE)) {
            // Nothing else to do.
            return;
        }
        foreach ($branches as $branch_name => $branch_config) {
            $this->newBranchTarget($branch_name);
        }
        // Inter-db graph stats if at least a couple of branches declared.
        if ($this->hasFeature('full', self::FEATURE_GRAPH_STATS) && $branches >= 2) {
            $this->newInterDatabaseGraphsStatsTarget($branches);
        }
    }

    /**
     * Fills default values on makefile representation.
     */
    protected function buildBaseMakefile()
    {
        // First level variables.
        $simple_variable_defaults = array(
            'php' => array('variable_name' => 'PHP', 'value' => 'php'),
            'analyzer' => array('variable_name' => 'ANALYZER', 'value' => '$(PHP) ../../analyzer.php'),
            'filter_files' => array('variable_name' => 'FILTER_FILES', 'value' => '../../src/filter-to-file-activity.sed'),
            'gendb' => array('variable_name' => 'GENDB', 'value' => '$(ANALYZER) gendb --mail-to-contributor-file="config/mail-map.yml" --commit-override-file="config/commit_messages_override.yml"'),
            'genstats' => array('variable_name' => 'GENSTATS', 'value' => '$(ANALYZER) genstatsyml'),
            'filter_min' => array('variable_name' => 'FILTER_MIN', 'value' => '../../src/filter_stat_by_min_amount.sh'),
            'gencodeswarmconfig' => array('variable_name' => 'GENCODESWARMCONFIG', 'value' => '$(ANALYZER) gencodeswarmconfig config/analyzer.yml'),
            'twig_templates_dir' => array('variable_name' => 'TWIG_TEMPLATES', 'value' => '../../src/templates'),
            'gencodeswarmxml' => array('variable_name' => 'GENCODESWARMXML', 'value' => '$(ANALYZER) gencodeswarmxml --template-dir="$(TWIG_TEMPLATES)"'),
            'genstatshtml' => array('variable_name' => 'GENSTATS_HTML', 'value' => '$(ANALYZER) genstatshtml --template-dir="$(TWIG_TEMPLATES)"'),
            'gengraphshtml' => array('variable_name' => 'GENGRAPHSTATS', 'value' => '$(ANALYZER) gengraphshtml --template-dir="$(TWIG_TEMPLATES)" --config-file="config/analyzer.yml"'),
            'geninterdbgraphshtml' => array('variable_name' => 'GENINTERDBGRAPHSTATS', 'value' => '$(ANALYZER) geninterdbgraphshtml --template-dir="$(TWIG_TEMPLATES)" --config-file="config/analyzer.yml"'),
        );
        foreach ($simple_variable_defaults as $yaml_key => $default) {
            $this->makefile['variables'][$default['variable_name']] = $this->configuration->getOption($yaml_key, $default['value']);
        }
        $optional_hard_variables = array(
            'shell' => array('variable_name' => 'SHELL', 'value' => '/bin/sh'),
        );
        foreach ($optional_hard_variables as $yaml_key => $default) {
            if ($this->configuration->getOption($yaml_key, FALSE)) {
                $this->makefile['hard_variables'][$default['variable_name']] = $this->configuration->getOption($yaml_key, FALSE);
            }
        }

        // Some base targets.
        $this->makefile['targets'] = array(
            'all' => array(
                'dependencies' => array('dbs', 'stats', 'graphstats', 'tagclouds', 'codeswarm'),
            ),
            'clean' => array(
                'commands' => array('rm -rf data'),
            ),
            'project.git' => array(
                'commands' => array(
                    sprintf('git clone --mirror %s project.git', escapeshellarg($this->configuration->getOption('git_url', ''))),
                ),
            ),
        );
        $this->makefile['targets']['.PHONY'] = array(
            'dependencies' => array('all', 'clean', 'dbs', 'stats', 'graphstats', 'tagclouds', 'codeswarm'),
        );

        // Some base directories.
        foreach (array('dbs', 'stats', 'tagclouds', 'codeswarm', 'graphstats') as $directory_name) {
            $this->newTargetDirectory("data/$directory_name");
        }

        // tagcloud related variables.
        $tagcloud_variable_defaults = array(
            'app' => array('variable_name' => 'TAGCLOUD_APP', 'value' => './../../dist/TagCloud/TagCloud'),
            'min_number' => array('variable_name' => 'TAGCLOUD_MINNUMBER', 'value' => 1),
            'width' => array('variable_name' => 'TAGCLOUD_WIDTH', 'value' => '1024'),
            'height' => array('variable_name' => 'TAGCLOUD_HEIGHT', 'value' => '768'),
            'fontsize_min' => array('variable_name' => 'TAGCLOUD_FONT_SIZE_MIN', 'value' => 8),
            'fontsize_max' => array('variable_name' => 'TAGCLOUD_FONT_SIZE_MAX', 'value' => 40),
            'args' => array('variable_name' => 'TAGCLOUD_ARGS', 'value' => 'destination=data/tagclouds width=$(TAGCLOUD_WIDTH) height=$(TAGCLOUD_HEIGHT) font_size_min=$(TAGCLOUD_FONT_SIZE_MIN) font_size_max=$(TAGCLOUD_FONT_SIZE_MAX)'),
        );
        foreach ($tagcloud_variable_defaults as $yaml_key => $default) {
            $this->makefile['variables'][$default['variable_name']] = $this->configuration->getOption('tagcloud', $yaml_key, $default['value']);
        }
    }

    /**
     * Produces a branch target identifier string.
     *
     * @param string $branch_id
     *   git branch name.
     *
     * @return string
     *   Branch target identifier string.
     */
    protected function getBranchTargetId($branch_id) {
        return $this->configuration->getOption('project', '') . '-' . $branch_id;
    }

    /**
     * Adds several targets related to the passed branch.
     *
     * @param string $branch_id
     *   git branch name or 'full' representing the whole git history.
     */
    protected function newBranchTarget($branch_id)
    {
        $id = $this->getBranchTargetId($branch_id);
        $maintainers = $this->configuration->getBranchMaintainers($branch_id);
        // Database.
        if ($this->hasFeature($branch_id, self::FEATURE_DB)) {
            $db = $this->newDatabaseTarget($branch_id);
        }
        // Stats.
        if ($this->hasFeature($branch_id, self::FEATURE_STATS)) {
            $file_changes_stat = $this->newFileChangesStatTarget($branch_id, $db, $maintainers);
            $commits_stat = $this->newCommitsStatTarget($branch_id, $db, $maintainers);
            if (!empty($maintainers)) {
                $file_changes_no_maintainers_stat = $this->newFileChangesNoMaintainersStatTarget($branch_id, $file_changes_stat, $maintainers);
                $commits_no_maintainers_stat = $this->newCommitsNoMaintainersStatTarget($branch_id, $commits_stat, $maintainers);
            }
        }
        // Graph stats.
        if ($this->hasFeature($branch_id, self::FEATURE_GRAPH_STATS)) {
            $this->newGraphStatsTarget($branch_id, $db, $maintainers);
        }
        // Tag clouds.
        if ($this->hasFeature($branch_id, self::FEATURE_TAGCLOUDS)) {
            $this->newTagcloudTarget($branch_id, 'dev-participation-by-file_changes', $file_changes_stat);
            $this->newTagcloudTarget($branch_id, 'dev-participation-by-commits', $commits_stat);
            if (!empty($maintainers)) {
                $this->newTagcloudTarget($branch_id, 'dev-participation-by-file_changes-no-maintainers', $file_changes_no_maintainers_stat);
                $this->newTagcloudTarget($branch_id, 'dev-participation-by-commits-no-maintainers', $commits_no_maintainers_stat);
            }
        }
        // Codeswarm videos.
        if ($this->hasFeature($branch_id, self::FEATURE_CODESWARM)) {
            $this->newCodeswarmTarget($branch_id, $db);
        }
    }

    /**
     * Adds a new sqlite3 database target for base information.
     *
     * @param string $branch_id
     *   git branch name.
     *
     * @return string
     *   Created makefile target.
     */
    protected function newDatabaseTarget($branch_id)
    {
        $id = $this->getBranchTargetId($branch_id);
        $db = sprintf('data/dbs/%s.sqlite3', $id);
        if ($branch_id == 'full') {
            $command = '$(GENDB) $< all $@';
        }
        else {
            $ref_interval = $this->configuration->getBranchOption($branch_id, 'start', FALSE) . '..' . $branch_id;
            $command = sprintf('$(GENDB) $< %s $@', escapeshellarg($ref_interval));
        }
        $this->makefile['targets'][$db] = array(
            'dependencies' => array('project.git', 'config/mail-map.yml', 'config/commit_messages_override.yml'),
            'order_dependencies' => array('data/dbs'),
            'commands' => array($command),
        );
        $this->makefile['targets']['dbs']['dependencies'][] = $db;
        return $db;
    }

    /**
     * Adds a new file changes statistic file target.
     *
     * @param string $branch_id
     *   git branch name.
     * @param boolean $db
     *   sqlite3 database file to use.
     * @param array $maintainers
     *   List of usernames to consider as maintainers.
     *
     * @return string
     *   Created makefile target.
     */
    protected function newFileChangesStatTarget($branch_id, $db, $maintainers)
    {
        $id = $this->getBranchTargetId($branch_id);
        $file_changes_stat = sprintf('data/stats/%s-dev-participation-by-file-changes.yml', $id);
        $this->makefile['targets'][$file_changes_stat] = array(
            'dependencies' => array($db),
            'order_dependencies' => array('data/stats'),
            'commands' => array('$(GENSTATS) $< file-changes > $@'),
        );
        $this->makefile['targets']['stats']['dependencies'][] = $file_changes_stat;
        $this->newHtmlStat($file_changes_stat);
        return $file_changes_stat;
    }

    /**
     * Adds a new file changes with not maintainers statistic file target.
     *
     * @param string $branch_id
     *   git branch name.
     * @param boolean $file_changes_stat
     *   File changes stat target.
     * @param array $maintainers
     *   List of usernames to consider as maintainers.
     *
     * @return string
     *   Created makefile target.
     */
    protected function newFileChangesNoMaintainersStatTarget($branch_id, $file_changes_stat, $maintainers)
    {
        $id = $this->getBranchTargetId($branch_id);
        $full_maintainers_egrep_filter = sprintf('(%s)', implode('|', $maintainers));
        $file_changes_no_maintainers_stat = sprintf('data/stats/%s-dev-participation-by-file-changes-no-maintainers.yml', $id);
        $this->makefile['targets'][$file_changes_no_maintainers_stat] = array(
            'dependencies' => array($file_changes_stat),
            'order_dependencies' => array('data/stats'),
            'commands' => array(sprintf('egrep -v %s $< > $@', escapeshellarg($full_maintainers_egrep_filter))),
        );
        $this->makefile['targets']['stats']['dependencies'][] = $file_changes_no_maintainers_stat;
        $this->newHtmlStat($file_changes_no_maintainers_stat);
        return $file_changes_no_maintainers_stat;
    }

    /**
     * Adds a new commits statistic file target.
     *
     * @param string $branch_id
     *   git branch name.
     * @param boolean $db
     *   sqlite3 database file to use.
     * @param array $maintainers
     *   List of usernames to consider as maintainers.
     *
     * @return string
     *   Created makefile target.
     */
    protected function newCommitsStatTarget($branch_id, $db, $maintainers)
    {
        $id = $this->getBranchTargetId($branch_id);
        $commits_stat = sprintf('data/stats/%s-dev-participation-by-commits.yml', $id);
        $this->makefile['targets'][$commits_stat] = array(
            'dependencies' => array($db),
            'order_dependencies' => array('data/stats'),
            'commands' => array('$(GENSTATS) $< commits > $@'),
        );
        $this->makefile['targets']['stats']['dependencies'][] = $commits_stat;
        $this->newHtmlStat($commits_stat);
        return $commits_stat;
    }

    /**
     * Adds a new commits with not maintainers statistic file target.
     *
     * @param string $branch_id
     *   git branch name.
     * @param boolean $commits_stat
     *   File changes stat target.
     * @param array $maintainers
     *   List of usernames to consider as maintainers.
     *
     * @return string
     *   Created makefile target.
     */
    protected function newCommitsNoMaintainersStatTarget($branch_id, $commits_stat, $maintainers)
    {
        $id = $this->getBranchTargetId($branch_id);
        $full_maintainers_egrep_filter = sprintf('(%s)', implode('|', $maintainers));
        $commits_no_maintainers_stat = sprintf('data/stats/%s-dev-participation-by-commits-no-maintainers.yml', $id);
        $this->makefile['targets'][$commits_no_maintainers_stat] = array(
            'dependencies' => array($commits_stat),
            'order_dependencies' => array('data/stats'),
            'commands' => array(sprintf('egrep -v %s $< > $@', escapeshellarg($full_maintainers_egrep_filter))),
        );
        $this->makefile['targets']['stats']['dependencies'][] = $commits_no_maintainers_stat;
        $this->newHtmlStat($commits_no_maintainers_stat);
        return $commits_no_maintainers_stat;
    }

    /**
     * Adds base css on the passed directory.
     *
     * @param string $assets_dir
     *   Where to add them.
     */
    protected function newBootstrapCssTarget($assets_dir) {
        $this->newTargetDirectory($assets_dir);
        $bootstrap_css_target = "$assets_dir/bootstrap.min.css";
        if (!empty($this->makefile['targets'][$bootstrap_css_target])) {
            // Nothing to do.
            return $bootstrap_css_target;
        }
        $this->makefile['targets'][$bootstrap_css_target] = array(
            'dependencies' => array('../../dist/bootstrap.zip'),
            'order_dependencies' => array($assets_dir),
            'commands' => array(
                'unzip -p ../../dist/bootstrap.zip dist/css/bootstrap.min.css > $@'
            ),
        );
        return $bootstrap_css_target;
    }

    /**
     * Adds a directory target to makefile.
     */
    protected function newHtmlStat($stat)
    {
        $stat_html = preg_replace('/.yml$/', '.html/', $stat);
        $assets_dir = 'data/stats/assets';
        $bootstrap_css_target = $this->newBootstrapCssTarget($assets_dir);
        $this->makefile['targets'][$stat_html] = array(
            'dependencies' => array($stat, $bootstrap_css_target),
            'commands' => array('$(GENSTATS_HTML) $< > $@'),
        );
        $this->makefile['targets']['stats']['dependencies'][] = $stat_html;
    }

    /**
     * Adds tagcloud related targets to makefile.
     *
     * @todo Actually use all possible configuration options passed from yml.
     *
     * @param string $branch_id
     *   git branch name.
     * @param string $type_id
     *   Identifier to use in the file name.
     * @param array $stat
     *   Stat target.
     */
    protected function newTagcloudTarget($branch_id, $type_id, $stat)
    {
        $id = $this->getBranchTargetId($branch_id);
        $target = sprintf('data/tagclouds/%s-%s.tif', $id, $type_id);
        $min_variable_name = sprintf('TAGCLOUD_%s_MINNUMBER', $branch_id);
        $this->makefile['variables'][$min_variable_name] = $this->configuration->getTagcloudOption($branch_id, 'min_number', '$(TAGCLOUD_MINNUMBER)');
        $tagcloud_args_variable_name = sprintf('TAGCLOUD_%s_ARGS', $branch_id);
        $this->makefile['variables'][$tagcloud_args_variable_name] = $this->configuration->getTagcloudOption($branch_id, 'args', '$(TAGCLOUD_ARGS)');
        if ($display = $this->configuration->getOption('display', '')) {
            $display = sprintf('DISPLAY=%s ', escapeshellarg($display));
        }
        $tmp_file = sprintf('data/tagclouds/%s-%s', $id, $type_id);
        $this->makefile['targets'][$target] = array(
            'dependencies' => array($stat),
            'order_dependencies' => array('data/tagclouds'),
            'commands' => array(
                sprintf('./$(FILTER_MIN) $< $(%s) > %s', $min_variable_name, $tmp_file),
                sprintf('%s$(TAGCLOUD_APP) file=%s $(%s)', $display, $tmp_file, $tagcloud_args_variable_name),
                sprintf('rm %s', $tmp_file),
            ),
        );
        $this->makefile['targets']['tagclouds']['dependencies'][] = $target;
    }

    /**
     * Adds codewswarm related targets to makefile.
     *
     * @param string $branch_id
     *   git branch name.
     * @param string $db_file
     *   Identifier to use in the file name.
     *
     * @todo Actually use all possible codeswarm options.
     */
    protected function newCodeswarmTarget($branch_id, $db_file)
    {
        $id = $this->getBranchTargetId($branch_id);
        $codeswarm_xml = sprintf('data/codeswarm/%s.xml', $id);
        $this->makefile['targets'][$codeswarm_xml] = array(
            'dependencies' => array($db_file),
            'order_dependencies' => array('data/codeswarm'),
            'commands' => array('$(GENCODESWARMXML) $< > $@'),
        );
        $codeswarm_config = sprintf('data/codeswarm/%s.config', $id);
        $this->makefile['targets'][$codeswarm_config] = array(
            'order_dependencies' => array('data/codeswarm'),
            'commands' => array(sprintf('$(GENCODESWARMCONFIG) %s > $@', escapeshellarg($id))),
        );
        $this->makefile['targets']['codeswarm']['dependencies'][] = $codeswarm_xml;
        $this->makefile['targets']['codeswarm']['dependencies'][] = $codeswarm_config;
        $code_path = 'data/codeswarm/code';
        if (empty($this->makefile['targets'][$code_path])) {
          $this->makefile['targets'][$code_path] = array(
            'dependencies' => array('../../dist/codeswarm-0.1.tar.gz'),
            'order_dependencies' => array('data/codeswarm'),
            'commands' => array(
                sprintf('mkdir -p %s', escapeshellarg($code_path)),
                sprintf('tar --strip-components=1 --extract --file=../../dist/codeswarm-0.1.tar.gz --directory=%s', escapeshellarg($code_path)),
                sprintf('mkdir -p %s', escapeshellarg("$code_path/frames")),
            ),
          );
        }
        if ($display = $this->configuration->getOption('display', '')) {
            $display = sprintf('DISPLAY=%s ', escapeshellarg($display));
        }
        $frames = sprintf('codeswarm-frames-%s', $id);
        $this->makefile['targets']['.PHONY']['dependencies'][] = $frames;
        $this->makefile['targets'][$frames] = array(
            'one_shell' => TRUE,
            'dependencies' => array($codeswarm_xml, $codeswarm_config, $code_path),
            'order_dependencies' => array('data/codeswarm'),
            'commands' => array(
                sprintf('cp %s %s/', escapeshellarg($codeswarm_config), escapeshellarg($code_path)),
                sprintf('cp %s %s/', escapeshellarg($codeswarm_xml), escapeshellarg($code_path)),
                sprintf('cd %s', escapeshellarg($code_path)),
                'echo `pwd`',
                sprintf('mkdir -p frames/%s', escapeshellarg($id)),
                sprintf('%sbash run.sh %s', $display, escapeshellarg(basename($codeswarm_config))),
            ),
        );
        $video_types = $this->configuration->getCodeswarmOption($branch_id, 'video_types', array('mp4'));
        if (in_array('flv', $video_types)) {
            $flv_video_name = sprintf('%s.flv', $id);
            $flv_video = "data/codeswarm/$flv_video_name";
            $this->makefile['targets'][$flv_video] = array(
                'one_shell' => TRUE,
                'dependencies' => array($frames),
                'commands' => array(
                    sprintf('cd %s', escapeshellarg("$code_path/frames/$id")),
                    sprintf("avconv -f image2 -i 'frame-%%06d.png' -an %s", escapeshellarg("../../../$flv_video_name")),
                ),
            );
            $this->makefile['targets']['codeswarm']['dependencies'][] = $flv_video;
        }
        if (in_array('avi', $video_types)) {
            $avi_video_name = sprintf('%s.avi', $id);
            $avi_video = "data/codeswarm/$avi_video_name";
            $this->makefile['targets'][$avi_video] = array(
                'one_shell' => TRUE,
                'dependencies' => array($frames),
                'commands' => array(
                    sprintf('cd %s', escapeshellarg("$code_path/frames/$id")),
                    sprintf("avconv -f image2 -i 'frame-%%06d.png' -an %s", escapeshellarg("../../../$avi_video_name")),
                ),
            );
            $this->makefile['targets']['codeswarm']['dependencies'][] = $avi_video;
        }
        if (in_array('mp4', $video_types)) {
            $mp4_video_name = sprintf('%s.mp4', $id);
            $mp4_video = "data/codeswarm/$mp4_video_name";
            $this->makefile['targets'][$mp4_video] = array(
                'one_shell' => TRUE,
                'order_dependencies' => array($frames),
                'commands' => array(
                    sprintf('cd %s', escapeshellarg("$code_path/frames/$id")),
                    sprintf("avconv -f image2 -i 'frame-%%06d.png' -an -c:v libx264 -r 25 %s", escapeshellarg("../../../$mp4_video_name")),
                ),
            );
            $this->makefile['targets']['codeswarm']['dependencies'][] = $mp4_video;
        }
    }

    /**
     * Adds a new html file target for related database graphs.
     *
     * @param string $branch_id
     *   git branch name.
     * @param string $db
     *   sqlite3 database file to use.
     * @param array $maintainers
     *   List of usernames to consider as maintainers.
     */
    protected function newGraphStatsTarget($branch_id, $db, $maintainers)
    {
        $id = $this->getBranchTargetId($branch_id);
        $graph_stat = sprintf('data/graphstats/%s-graphs.html', $id);
        $assets_dir = 'data/graphstats/assets';
        $jquery_target = "$assets_dir/jquery.js";
        $jquery_flot_target = "$assets_dir/jquery.flot.js";
        $jquery_flot_time_target = "$assets_dir/jquery.flot.time.js";
        $jquery_flot_axislabels_target = "$assets_dir/jquery.flot.axislabels.js";
        if (empty($this->makefile['targets'][$assets_dir])) {
            $this->newTargetDirectory($assets_dir);
            $this->makefile['targets'][$jquery_target] = array(
                'dependencies' => array('../../dist/flot.zip'),
                'order_dependencies' => array($assets_dir),
                'commands' => array(
                    'unzip -p ../../dist/flot.zip flot-master/jquery.js > $@'
                ),
            );
            $this->makefile['targets'][$jquery_flot_target] = array(
                'dependencies' => array('../../dist/flot.zip'),
                'order_dependencies' => array($assets_dir),
                'commands' => array(
                    'unzip -p ../../dist/flot.zip flot-master/jquery.flot.js > $@'
                ),
            );
            $this->makefile['targets'][$jquery_flot_time_target] = array(
                'dependencies' => array('../../dist/flot.zip'),
                'order_dependencies' => array($assets_dir),
                'commands' => array(
                    'unzip -p ../../dist/flot.zip flot-master/jquery.flot.time.js > $@'
                ),
            );
            $this->makefile['targets'][$jquery_flot_axislabels_target] = array(
                'dependencies' => array('../../dist/flot-axislabels.zip'),
                'order_dependencies' => array($assets_dir),
                'commands' => array(
                    'unzip -p ../../dist/flot-axislabels.zip flot-axislabels-master/jquery.flot.axislabels.js > $@'
                ),
            );
        }
        $bootstrap_css_target = $this->newBootstrapCssTarget($assets_dir);
        $this->makefile['targets'][$graph_stat] = array(
            'dependencies' => array($db, $bootstrap_css_target, $jquery_target, $jquery_flot_target, $jquery_flot_time_target, $jquery_flot_axislabels_target),
            'order_dependencies' => array('data/graphstats'),
            'commands' => array(
                sprintf('$(GENGRAPHSTATS) --identifier=%s %s > $@', escapeshellarg($id), escapeshellarg("$branch_id:$db")),
            ),
        );
        $this->makefile['targets']['graphstats']['dependencies'][] = $graph_stat;
        if (!empty($maintainers)) {
            $graph_stat_no_maintainers = sprintf('data/graphstats/%s-graphs-no-maintainers.html', $id);
            $this->makefile['targets'][$graph_stat_no_maintainers] = array(
                'dependencies' => array($db, $bootstrap_css_target, $jquery_target, $jquery_flot_target, $jquery_flot_time_target, $jquery_flot_axislabels_target),
                'order_dependencies' => array('data/graphstats'),
                'commands' => array(
                    sprintf('$(GENGRAPHSTATS) --identifier=%s --exclude-maintainers %s > $@', escapeshellarg("$id (no maintainers)"), escapeshellarg("$branch_id:$db")),
                ),
            );
            $this->makefile['targets']['graphstats']['dependencies'][] = $graph_stat_no_maintainers;
        }
        return $graph_stat;
    }

    /**
     * Adds a directory target to makefile.
     */
    protected function newTargetDirectory($directory)
    {
        if (!empty($this->makefile['targets'][$directory])) {
            // Nothing to do.
            return;
        }
        $this->makefile['targets'][$directory] = array(
            'commands' => array(
                sprintf('mkdir -p %s', escapeshellarg($directory)),
            ),
        );
    }

    protected function newInterDatabaseGraphsStatsTarget($branches) {
        $interdb_graph_stat = 'data/graphstats/multibranch-graphs.html';
        $dependencies = array();
        $order_dependencies = array();
        $db_files_arguments = array();
        foreach ($branches as $branch_id => $branch_config) {
            $id = $this->getBranchTargetId($branch_id);
            $db = sprintf('data/dbs/%s.sqlite3', $id);
            $dependencies[] = $db;
            $db_files_arguments[] = escapeshellarg("$branch_id:$db");
            $order_dependencies[] = sprintf('data/graphstats/%s-graphs.html', $id);
        }
        $this->makefile['targets'][$interdb_graph_stat] = array(
            'dependencies' => $dependencies,
            'order_dependencies' => $order_dependencies,
            'commands' => array(
                sprintf('$(GENINTERDBGRAPHSTATS) --identifier="Multi-branch graphs" %s > $@', implode(' ', $db_files_arguments))
            ),
        );
        $this->makefile['targets']['graphstats']['dependencies'][] = $interdb_graph_stat;
        $interdb_graph_stat_no_maintainers = 'data/graphstats/multibranch-graphs-no-maintainers.html';
        $this->makefile['targets'][$interdb_graph_stat_no_maintainers] = array(
            'dependencies' => $dependencies,
            'order_dependencies' => $order_dependencies,
            'commands' => array(
                sprintf('$(GENINTERDBGRAPHSTATS) --identifier="Multi-branch graphs" --exclude-maintainers %s > $@', implode(' ', $db_files_arguments))
            ),
        );
        $this->makefile['targets']['graphstats']['dependencies'][] = $interdb_graph_stat_no_maintainers;
    }

    /**
     * Reads the internal makefile and writes it to output.
     */
    protected function writeMakefile(OutputInterface $output)
    {
        $makefile = '';
        foreach ($this->makefile['hard_variables'] as $name => $value) {
            $makefile .= sprintf('%s = %s%s', $name, $value, PHP_EOL);
        }
        foreach ($this->makefile['variables'] as $name => $value) {
            $makefile .= sprintf('%s ?= %s%s', $name, $value, PHP_EOL);
        }
        foreach ($this->makefile['targets'] as $name => $target) {
            $dependencies = $commands = '';
            if (!empty($target['dependencies'])) {
                $dependencies .= ' ' . implode(' ', $target['dependencies']);
            }
            if (!empty($target['order_dependencies'])) {
                $dependencies .= ' | ' . implode(' ', $target['order_dependencies']);
            }
            if (!empty($target['one_shell'])) {
                $commands_separator = ';';
                $commands_group_prefix = "\t";
                $commands_prefix = '';
                $commands_group_suffix = PHP_EOL;
            }
            else {
                $commands_separator = PHP_EOL;
                $commands_group_prefix = '';
                $commands_prefix = "\t";
                $commands_group_suffix = '';
            }
            if (!empty($target['commands'])) {
                $commands .= $commands_group_prefix;
                foreach ($target['commands'] as $command) {
                    $commands .= sprintf('%s%s%s', $commands_prefix, $command, $commands_separator);
                }
                $commands .= $commands_group_suffix;
            }
            $makefile .= sprintf('%s:%s%s', $name, $dependencies, PHP_EOL);
            $makefile .= $commands;
        }
        $output->write($makefile);
    }

}
