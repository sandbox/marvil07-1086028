<?php

namespace DrupalContributionAnalyzer\Console\Command;

use DrupalContributionAnalyzer\DbAnalyzer;
use DrupalContributionAnalyzer\TwigAwareCommandTrait;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Produces an HTML version of the base stats file.
 */
class CodeswarmXmlGeneratorCommand extends Command
{

    use TwigAwareCommandTrait;

    /**
     * Object to interect with related databse.
     *
     * @var DbAnalyzer
     */
    protected $dbAnalyzer;

    protected function configure()
    {
        $this
            ->setName('gencodeswarmxml')
            ->setDescription('Generate codeswarm XML for an scenario.')
            ->addArgument(
                'sqlite-database-file',
                InputArgument::REQUIRED,
                'The database file.'
            )
            ->twigConfigure();
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->twigValidate($input);
        $db_file = $input->getArgument('sqlite-database-file');
        $this->dbAnalyzer = new DbAnalyzer($db_file);
        $variables = array();
        $variables['contributions'] = $this
            ->dbAnalyzer
            ->getDb()
            ->query(DbAnalyzer::QUERY_ALL);
        $this->twigRender($output, 'codeswarm.xml', $variables);
    }

}
