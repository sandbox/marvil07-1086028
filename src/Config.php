<?php

namespace DrupalContributionAnalyzer;

use Symfony\Component\Yaml\Yaml;

/**
 * General configuration.
 */
class Config
{

    /**
     * YAML loaded data from config-file argument.
     *
     * @var array
     */
    protected $configuration;

    /**
     * Constructor.
     *
     * @param $string $configuration_file
     *   The configuration file to use.
     *
     * @throws InvalidArgumentException|\Symfony\Component\Yaml\Exception\ParseException
     *   If file does not exists or connection failed to be created.
     */
    public function __construct($configuration_file)
    {
        if (!file_exists($configuration_file)) {
            throw new \InvalidArgumentException(sprintf('Configuration file (%s) does not exists', $configuration_file));
        }
        $this->configuration = Yaml::parse($configuration_file);
        $this->validateConfiguration();
    }

    /**
     * Gets the db object.
     *
     * @return PDO
     */
    public function getRaw() {
        return $this->configuration;
    }

    protected function checkArgumentsNumber($arguments, $min, $label = '')
    {
        if (count($arguments) < $min) {
            var_dump($arguments);
            throw new \RunTimeException(sprintf('%s: Not enough arguments provided.', $label));
        }
    }

    /**
     * Retrives an option from the configuration array.
     *
     * @param string $key,...
     *   Keys to use in each level of the configuration array.
     * @param string $default_value
     *   Default value to use if not found.
     *
     * @return mixed
     *   The option value or the passed default value if not found.
     *
     * @throws \RunTimeException
     *   If not called with the right amount of arguments.
     */
    public function getOption() {
        $keys = func_get_args();
        $this->checkArgumentsNumber($keys, 2, __METHOD__);
        $default_value = array_pop($keys);
        $value = $this->configuration;
        foreach ($keys as $key) {
            if (empty($value[$key])) {
                return $default_value;
            }
            $value = $value[$key];
        }
        return $value;
    }

    /**
     * Validates loaded configuration.
     */
    protected function validateConfiguration()
    {
        if (!is_array($this->configuration)) {
            throw new \InvalidArgumentException('Cannot parse YAML file correctly');
        }
        // Check required options.
        $required_keys = array('project', 'git_url');
        foreach ($required_keys as $required_key) {
            if (!$this->getOption($required_key, FALSE)) {
                throw new \InvalidArgumentException(sprintf('config-file: Missing required key "%s".', $required_key));
            }
        }
        foreach ($this->getOption('branches', array()) as $branch_id => $branch_config) {
            if ($branch_id != 'full' && empty($branch_config['start'])) {
                throw new \InvalidArgumentException(sprintf('config-file branch "%s" does contain start key', $branch_id));
            }
        }
    }

    /**
     * Retrieves a configuration option from branch context.
     *
     * @param string $branch_id
     *   Either the git branch name or 'full' referring to full history
     *   scenario.
     * @param string $key,...
     *   Keys to use in each level of the configuration array.
     * @param string $default_value
     *   Default value to use if not found.
     *
     * @return mixed
     *   The option value in the branch context. If not set there,
     *   retrieved from global context. If not there, use passed default value.
     *
     * @throws \RunTimeException
     *   If not called with the right amount of arguments.
     */
    public function getBranchOption($branch_id)
    {
        $keys = func_get_args();
        $this->checkArgumentsNumber($keys, 3, __METHOD__);
        // Add base key.
        array_unshift($keys, 'branches');
        if ($value = call_user_func_array(array($this, 'getOption'), $keys)) {
            return $value;
        }
        // Look for it in global context.
        array_shift($keys);
        array_shift($keys);
        return call_user_func_array(array($this, 'getOption'), $keys);
    }

    /**
     * Retrieves a tagcloud configuration option.
     *
     * @param string $branch_id
     *   Either the git branch name or 'full' referring to full history
     *   scenario.
     * @param string $key,...
     *   Keys to use in each level of the configuration array.
     * @param string $default_value
     *   Default value to use if not found.
     *
     * @return mixed
     *   The option value in the branch context. If not set there,
     *   retrieved from global context. If not there, use passed default value.
     *
     * @throws \RunTimeException
     *   If not called with the right amount of arguments.
     */
    public function getTagcloudOption($branch_id)
    {
        $keys = func_get_args();
        $this->checkArgumentsNumber($keys, 3, __METHOD__);
        // Add base key as second element.
        array_shift($keys);
        array_unshift($keys, 'tagcloud');
        array_unshift($keys, $branch_id);
        return call_user_func_array(array($this, 'getBranchOption'), $keys);
    }

    /**
     * Retrieves a codeswarm configuration option.
     *
     * @param string $branch_id
     *   Either the git branch name or 'full' referring to full history
     *   scenario.
     * @param string $key,...
     *   Keys to use in each level of the configuration array.
     * @param string $default_value
     *   Default value to use if not found.
     *
     * @return mixed
     *   The option value in the branch context. If not set there,
     *   retrieved from global context. If not there, use passed default value.
     *
     * @throws \RunTimeException
     *   If not called with the right amount of arguments.
     */
    public function getCodeswarmOption($branch_id)
    {
        $keys = func_get_args();
        $this->checkArgumentsNumber($keys, 3, __METHOD__);
        // Add base key as second element.
        array_shift($keys);
        array_unshift($keys, 'codeswarm');
        array_unshift($keys, $branch_id);
        return call_user_func_array(array($this, 'getBranchOption'), $keys);
    }

    /**
     * Gets related maintainers.
     *
     * @param string $branch_id
     *   Either the git branch name or 'full' referring to full history
     *   scenario.
     *
     * @return array
     *   List of d.o usernames which maitain related branch.
     */
    public function getBranchMaintainers($branch_id) {
        if ($branch_id == 'full') {
            $maintainers = $this->getOption('maintainers', array());
        }
        else {
            $maintainers = $this->getBranchOption($branch_id, 'maintainers', array());
        }
        return $maintainers;
    }

}
