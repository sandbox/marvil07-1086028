<?php

namespace DrupalContributionAnalyzer;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Adds twig related configuration.
 */
trait TwigAwareCommandTrait
{

    protected $twigTemplateDirectory = null;
    private $twigTemplateDirectoryValidated = FALSE;

    protected function twigConfigure()
    {
        $this
            ->addOption(
                'template-dir',
                't',
                InputOption::VALUE_REQUIRED,
                'The twig templates directory.',
                null
            );
    }

    protected function twigValidate(InputInterface $input)
    {
        $template_dir = $input->getOption('template-dir');
        if (is_null($template_dir)) {
            throw new \InvalidArgumentException('template-dir option is not provided.');
        }
        if (!is_dir($template_dir)) {
            throw new \InvalidArgumentException('template-dir is not a valid directory.');
        }
        $this->twigTemplateDirectory = $template_dir;
        $this->twigTemplateDirectoryValidated = TRUE;
        return $this->twigTemplateDirectory;
    }

    /**
     * Renders twig template to output.
     *
     * @param \Symfony\Component\Console\Output\OutputInterface $output
     *   Output object.
     * @param string $template_name
     *   Name of the twig template to use.
     * @param array $variables
     *   Variable sto pass to twig render.
     */
    protected function twigRender(OutputInterface $output, $template_name, $variables)
    {
        if (!$this->twigTemplateDirectoryValidated) {
            throw new \RuntimeException('TwigAwareCommandTrait validation was not called before calling rendering.');
        }
        $loader = new \Twig_Loader_Filesystem($this->twigTemplateDirectory);
        $twig = new \Twig_Environment($loader, array(
            'cache' => 'cache',
        ));
        $output->write($twig->render($template_name, $variables));
    }

}
