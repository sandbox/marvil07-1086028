<?php

namespace DrupalContributionAnalyzer;

/**
 * Groups base interaction with generated databases.
 */
class DbAnalyzer
{

    /**
     * Internal database object.
     *
     * @var PDO
     */
    protected $db;

    const QUERY_CREATE = '
        CREATE TABLE IF NOT EXISTS contributions (
          id INTEGER PRIMARY KEY,
          commit_hash TEXT,
          author_timestamp INTEGER,
          filename TEXT,
          contributor TEXT)';
    const QUERY_ALL = 'SELECT * FROM contributions';
    const QUERY_COMMITS_BY_CONTRIBUTOR = '
        SELECT contributor, COUNT(DISTINCT commit_hash) as value
        FROM contributions
        GROUP BY contributor';
    const QUERY_COMMITS_BY_CONTRIBUTOR_NO_MAINTAINERS = '
        SELECT contributor, COUNT(DISTINCT commit_hash) as value
        FROM contributions
        WHERE contributor NOT IN (%s)
        GROUP BY contributor';
    const QUERY_FILECHANGES_BY_CONTRIBUTOR = '
        SELECT contributor, COUNT(filename) as value
        FROM contributions
        GROUP BY contributor';
    const QUERY_FILECHANGES_BY_CONTRIBUTOR_NO_MAINTAINERS = '
        SELECT contributor, COUNT(filename) as value
        FROM contributions
        WHERE contributor NOT IN (%s)
        GROUP BY contributor';
    const QUERY_CONTRIBUTORS_BY_COMMIT = '
        SELECT COUNT(DISTINCT contributor) as value
        FROM contributions
        GROUP BY commit_hash';
    const QUERY_CONTRIBUTORS_BY_COMMIT_NO_MAINTAINERS = '
        SELECT COUNT(DISTINCT contributor) as value
        FROM contributions
        WHERE contributor NOT IN (%s)
        GROUP BY commit_hash';
    const QUERY_FILECHANGES_BY_COMMIT = '
        SELECT COUNT(DISTINCT filename) as value
        FROM contributions
        GROUP BY commit_hash';
    const QUERY_FILECHANGES_BY_COMMIT_NO_MAINTAINERS = '
        SELECT COUNT(DISTINCT filename) as value
        FROM contributions
        WHERE contributor NOT IN (%s)
        GROUP BY commit_hash';
    const QUERY_COMMITS_PER_MONTH = "
        SELECT strftime('%s', datetime(author_timestamp, 'unixepoch'), 'start of month') as date_point, COUNT(DISTINCT commit_hash) as value
        FROM contributions
        GROUP BY date_point";
    // Note the extra % to avoid sprintf to recognize a string placeholder used by sql.
    const QUERY_COMMITS_PER_MONTH_NO_MAINTAINERS = "
        SELECT strftime('%%s', datetime(author_timestamp, 'unixepoch'), 'start of month') as date_point, COUNT(DISTINCT commit_hash) as value
        FROM contributions
        WHERE contributor NOT IN (%s)
        GROUP BY date_point";
    const QUERY_CONTRIBUTORS_PER_MONTH = "
        SELECT strftime('%s', datetime(author_timestamp, 'unixepoch'), 'start of month') as date_point, COUNT(DISTINCT contributor) as value
        FROM contributions
        GROUP BY date_point";
    const QUERY_CONTRIBUTORS_PER_MONTH_NO_MAINTAINERS = "
        SELECT strftime('%%s', datetime(author_timestamp, 'unixepoch'), 'start of month') as date_point, COUNT(DISTINCT contributor) as value
        FROM contributions
        WHERE contributor NOT IN (%s)
        GROUP BY date_point";

    /**
     * Constructor.
     *
     * @param $string $db_file
     *   The database file to use.
     *
     * @throws InvalidArgumentException|PDOException
     *   If file does not exists or connection failed to be created.
     */
    public function __construct($db_file)
    {
        if (!file_exists($db_file)) {
            throw new \InvalidArgumentException(sprintf('sqlite-database-file (%s) does not exists', $db_file));
        }
        $connection_string = sprintf('sqlite:%s', $db_file);
        $this->db = new \PDO($connection_string);
    }

    /**
     * Creates the base tables in the database.
     *
     * @return \DrupalContributionAnalyzer\DbAnalyzer
     *   The created object.
     */
    public static function create($db_file)
    {
        $connection_string = sprintf('sqlite:%s', $db_file);
        $db = new \PDO($connection_string);
        $db->exec(self::QUERY_CREATE);
        return new self($db_file);
    }

    /**
     * Gets the db object.
     *
     * @return PDO
     */
    public function getDb() {
        return $this->db;
    }

    /**
     * Helper to get a no-maintainer query.
     *
     * @param string $with_maintainers_query
     *   Related query including maintainers.
     * @param array $maintainers
     *   List of maintainer.
     *
     * @return \PDOStatement
     *   Requested object.
     */
    public function getNoMaintainersQuery($with_maintainers_query, $maintainers) {
        if (empty($maintainers)) {
            throw new \Exception('Cannot get a non-maintainer query with empty maintainers.');
        }
        switch ($with_maintainers_query) {
            case self::QUERY_COMMITS_BY_CONTRIBUTOR:
                $query_base = self::QUERY_COMMITS_BY_CONTRIBUTOR_NO_MAINTAINERS;
                break;
            case self::QUERY_FILECHANGES_BY_CONTRIBUTOR:
                $query_base = self::QUERY_FILECHANGES_BY_CONTRIBUTOR_NO_MAINTAINERS;
                break;
            case self::QUERY_CONTRIBUTORS_BY_COMMIT:
                $query_base = self::QUERY_CONTRIBUTORS_BY_COMMIT_NO_MAINTAINERS;
                break;
            case self::QUERY_FILECHANGES_BY_COMMIT:
                $query_base = self::QUERY_FILECHANGES_BY_COMMIT_NO_MAINTAINERS;
                break;
            case self::QUERY_COMMITS_PER_MONTH:
                $query_base = self::QUERY_COMMITS_PER_MONTH_NO_MAINTAINERS;
                break;
            case self::QUERY_CONTRIBUTORS_PER_MONTH:
                $query_base = self::QUERY_CONTRIBUTORS_PER_MONTH_NO_MAINTAINERS;
                break;
            default:
                throw new \Exception(sprintf('Query does not have non-maitainers related query: %s', $with_maintainers_query));
        }
        $maintainers_placeholder = implode(',', array_fill(0, count($maintainers), '?'));
        $query_string = sprintf($query_base, $maintainers_placeholder);
        $statement = $this->db->prepare($query_string);
        foreach ($maintainers as $index => $maintainer) {
            $statement->bindValue($index+1, $maintainer);
        }
        return $statement;
    }

}
