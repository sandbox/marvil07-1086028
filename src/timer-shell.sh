#!/bin/sh
# Remove make '-c'.
shift
# time command has human readable output: different format depending on the
# value. Let's do it manually to have seconds.
START=`date +%s`
sh -c "$@"
END=`date +%s`
SECS=$(($END - $START))
echo "$@\t$SECS" >> .log
