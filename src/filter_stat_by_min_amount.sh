#!/bin/sh

usage() {
	echo "$0 stats_file min_number"
}

if [ "$#" != "2" ]; then
	echo error: you need to pass 2 parameters
	usage
	exit 1
fi

STATS_FILE=$1
MIN_NUMBER=$2

awk -F: -v min=$MIN_NUMBER '{ if ( $2 > min ) print $0  }' $STATS_FILE 
