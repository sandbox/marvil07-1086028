PROCESSING_SKETCH ?= 'src/TagCloud'

all: doc dist-init
dist-init: dist/TagCloud dist/codeswarm-0.1.tar.gz dist/processing composer.lock dist/flot.zip dist/flot-axislabels.zip dist/bootstrap.zip
.PHONY: all doc clean dist-init

doc: README.html
README.html: README.txt overview.png
	asciidoc -a data-uri -a icons -a iconsdir=/etc/asciidoc/images/icons -o README.html README.txt
overview.png: overview.dot
	dot -Tpng -o overview.png overview.dot

dist:
	mkdir -p $@
dist/processing: | dist
ifeq ($(PROCESSING_DIR),)
	$(error Please provide the PROCESSING_DIR environment variable with the path to processing)
endif
	ln -s $(PROCESSING_DIR) $@
dist/TagCloud: | dist/processing
	$(PROCESSING_DIR)/processing-java \
		--sketch=$(PROCESSING_SKETCH) \
		--output=$@ \
		--export
dist/codeswarm-0.1.tar.gz: | dist
	curl https://codeswarm.googlecode.com/files/codeswarm-0.1.tar.gz > $@
dist/flot.zip: | dist
	curl https://codeload.github.com/flot/flot/zip/master > $@
dist/flot-axislabels.zip: | dist
	curl https://codeload.github.com/markrcote/flot-axislabels/zip/master > $@
dist/bootstrap.zip: | dist
	curl -L https://github.com/twbs/bootstrap/releases/download/v3.0.3/bootstrap-3.0.3-dist.zip > $@

composer.phar:
	curl https://getcomposer.org/composer.phar > $@
composer.lock: | composer.phar
	php composer.phar install --prefer-dist

clean:
	rm -rf README.html overview.png dist
